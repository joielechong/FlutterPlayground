import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material Button with No Padding',
      home: MyButton(),
    );
  }
}

class MyButton extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Material Button with No Padding')),
      body: Row(
        children: <Widget>[
          MaterialButton(
            padding: EdgeInsets.zero,
            minWidth: 0,
            child: Image.asset("images/male.png", width: 33),
            color: Colors.blue,
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
