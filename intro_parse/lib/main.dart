import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() async {
  List _data = await getJson();

  print(_data[0]['title']);

  for (int i = 0; i < _data.length; i++) {
    print("Title ${_data[i]['title']}");
    print("BOdy: ${_data[i]['body']}");
  }

  String _body = _data[0]['body'];

  runApp(new MaterialApp(
      home: new Scaffold(
    appBar: new AppBar(
      title: new Text("JSON Parse"),
      centerTitle: true,
      backgroundColor: Colors.pinkAccent,
    ),
    body: new Center(
        child: new ListView.builder(
            itemCount: _data.length,
            padding: const EdgeInsets.all(16.0),
            itemBuilder: (BuildContext context, int position) {
              return new ListTile(
                title: new Text("${_data[position]['title']}",
                    style: new TextStyle(fontSize: 18.0)),
                leading: new CircleAvatar(
                  backgroundColor: Colors.orangeAccent,
                  child: new Text(
                    "${_data[position]['title'][0]}",
                    style: new TextStyle(fontSize: 13.4, color: Colors.black38),
                  ),
                ),
                subtitle: new Text(
                  _data[position]['body'],
                  style: new TextStyle(
                      fontSize: 13.4,
                      color: Colors.grey,
                      fontStyle: FontStyle.italic),
                ),
                onTap: () {
                  _showTapOnMessage(context, "${_data[position]['title']}");
                },
              );
            })),
  )));
}

void _showTapOnMessage(BuildContext context, String message) {
  var alert = new AlertDialog(
    title: new Text('App'),
    content: new Text(message),
    actions: <Widget>[
      new FlatButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: new Text('OK'),
      )
    ],
  );


  showDialog(context: context, builder: (_)=> alert);
}

Future<List> getJson() async {
  String apiUrl = 'https://jsonplaceholder.typicode.com/posts';

  http.Response response = await http.get(apiUrl);

  return json.decode(response.body);
}
