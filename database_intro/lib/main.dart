import 'package:database_intro/utils/database_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/user.dart';

List _users;

void main() async {
  var db = new DatabaseHelper();

  // Add user
//  int savedUser = await db.saveUser(new User("Bertrand", "Russell"));
//  print("user saved $savedUser");

  int count = await db.getCount();
  print("Count: $count");

//  User malin = await db.getUser(2);
//  print("Got username: ${malin.username}");

  // Delete a user
//  int userDeleted = await db.deleteUser(3);
//  print("Delete user = $userDeleted");

//  User malin = await db.getUser(2);
//  User malinUpdated =
//      User.fromMap({"username": "malingg", "password": "kandang", "id": 2});

//  await db.updateUser(malinUpdated);

  // Get all users
  _users = await db.getAllUsers();

  for (int i = 0; i < _users.length; i++) {
    User user = User.map(_users[i]);

    print("Username: ${user.username}" + ", user id: ${user.id}");
  }

  runApp(new MaterialApp(
    title: "Database",
    home: new Home(),
  ));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Database"),
        centerTitle: true,
        backgroundColor: Colors.lightGreen,
      ),
      body: new Center(
        child: new ListView.builder(
          itemCount: _users.length,
          itemBuilder: (BuildContext context, int position) {
            return new Card(
                elevation: 2,
                color: Colors.white,
                child: new ListTile(
                  leading: new CircleAvatar(
                    child: new Text(User.map(_users[position]).username.substring(0,1)),
                  ),
                  title: new Text(User.map(_users[position]).username),
                  subtitle: new Text(User.map(_users[position]).password),
                  onTap: () {
                    print("clicked position = $position");
                  },
                ));
          },
        ),
      ),
    );
  }
}
