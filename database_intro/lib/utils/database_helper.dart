import 'dart:async';
import 'dart:io';

import 'package:database_intro/model/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  final String userTable = "userTable";
  final String columnId = "id";
  final String columnUsername = "username";
  final String columnPassword = "password";

  Future<Database> get db async {
    if (_db == null) {
      _db = await initDb();
    }

    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "maindb.db");

    // Make sure the directory exists
    try {
      await Directory(databasesPath).create(recursive: true);
    } catch (_) {}

    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE $userTable($columnId INTEGER PRIMARY KEY UNIQUE, $columnUsername TEXT, $columnPassword TEXT)");
  }

  Future<int> saveUser(User user) async {
    var dbClient = await db;

    int res = await dbClient.insert(userTable, user.toMap());
    return res;
  }

  Future<List> getAllUsers() async {
    var dbClient = await db;
    String sql = "SELECT * FROM $userTable";
    var result = await dbClient.rawQuery(sql);

    return result.toList();
  }

  Future<int> getCount() async {
    var dbClient = await db;
    String sql = "SELECT COUNT(*) FROM $userTable";
    return Sqflite.firstIntValue(await dbClient.rawQuery(sql));
  }

  Future<User> getUser(int id) async {
    var dbClient = await db;
    String sql = "SELECT * FROM $userTable WHERE $columnId = $id";
    var result = await dbClient.rawQuery(sql);
    if (result.length == 0) return null;
    return new User.fromMap(result.first);
  }

  Future<int> deleteUser(int id) async {
    var dbClient = await db;

    return await dbClient
        .delete(userTable, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    return await dbClient.update(userTable,
    user.toMap(), where: "$columnId = ?", whereArgs: [user.id]);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
