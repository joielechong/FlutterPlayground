import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:klimatic/util/util.dart';

class Klimatic extends StatefulWidget {
  @override
  _KlimaticState createState() => _KlimaticState();
}

class _KlimaticState extends State<Klimatic> {
  String _cityEntered;

  Future _gotoNextScreen(BuildContext context) async {
    Map result = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return ChangeCity();
    }));

    if (result != null && result.containsKey("enter")) {
//      print(result['enter']);
      _cityEntered = result['enter'];
    }
  }

  void showStuff() async {
    Map data = await getWeather(appId, defaultCity);
    print(data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Klimatic"),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              _gotoNextScreen(context);
            },
          )
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new Center(
            child: Image.asset('images/umbrella.png',
                width: 900, height: 900, fit: BoxFit.fill),
          ),
          new Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.fromLTRB(0.0, 10.9, 20.9, 0.0),
            child: Text(_cityEntered == null? defaultCity: _cityEntered,
                style: cityStyle()),
          ),
          new Container(
            alignment: Alignment.center,
            child: Image.asset('images/light_rain.png'),
          ),
          new Container(
              margin: EdgeInsets.fromLTRB(30.0, 100.0, 0, 0),
              alignment: Alignment.center,
              child: updateTempWidget(_cityEntered))
        ],
      ),
    );
  }

  Future<Map> getWeather(String appId, String city) async {
    String apiUrl =
        'http://api.openweathermap.org/data/2.5/weather?q=$city&appid=' +
            appId +
            "&units=imperial";

    http.Response response = await http.get(apiUrl);

    return json.decode(response.body);
  }

  Widget updateTempWidget(String city) {
    return new FutureBuilder(
        future: getWeather(appId, city == null ? defaultCity : city),
        builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
          if (snapshot.hasData) {
            Map content = snapshot.data;
            return new Container(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: Text(
                      content['main']['temp'].toString(),
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: 49.9,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),

                    subtitle: ListTile(
                      title: Text("Humidity: ${content['main']['humidity']}\n"
                      "Min: ${content['main']['temp_min']}\n"
                      "Max: ${content['main']['temp_max']}",
                      style: extraData(),),
                    ),
                  )
                ],
              ),
            );
          } else {
            return new Container();
          }
        });
  }

  TextStyle cityStyle() {
    return TextStyle(
        color: Colors.white, fontSize: 22.9, fontStyle: FontStyle.italic);
  }

  TextStyle extraData() {
    return TextStyle(
        color: Colors.white70,
        fontStyle: FontStyle.normal,
        fontSize: 17.0);
  }

  TextStyle tempStyle() {
    return TextStyle(
        color: Colors.white,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w500,
        fontSize: 49.9);
  }
}

class ChangeCity extends StatelessWidget {
  var _cityFieldController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.red,
        title: Text("Change city"),
        centerTitle: true,
      ),
      body: new Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Center(
                  child: Image.asset(
                'images/white_snow.png',
                width: 490,
                height: 600,
                fit: BoxFit.fill,
              ))
            ],
          ),
          ListView(
            children: <Widget>[
              ListTile(
                title: TextField(
                  decoration: InputDecoration(hintText: "Enter city"),
                  controller: _cityFieldController,
                  keyboardType: TextInputType.text,
                ),
              ),
              ListTile(
                title: FlatButton(
                  onPressed: () {
                    Navigator.of(context)
                        .pop({'enter': _cityFieldController.text});
                  },
                  textColor: Colors.white70,
                  color: Colors.redAccent,
                  child: Text("Get Weather"),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
