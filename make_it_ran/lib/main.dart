import 'package:flutter/material.dart';
import 'package:make_it_ran/ui/make_it_rain.dart';

void main() {
  runApp(new MaterialApp(
    title: "Make It Ran",
    home: new MakeItRain(),
  ));
}