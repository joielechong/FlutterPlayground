import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Make It Rain!"),
        backgroundColor: Colors.lightGreen,
      ),

      backgroundColor: Colors.grey.shade50,

      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () => debugPrint("Clicked!"),
      ),
    );
  }
}
