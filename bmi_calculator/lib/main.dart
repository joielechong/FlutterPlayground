import 'package:bmi_calculator/ui/home.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "BMI CALCULATOR",
    home: new Home(),
  ));
}