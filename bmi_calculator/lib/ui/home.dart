import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> {
  TextEditingController _ageController = new TextEditingController();
  TextEditingController _heightController = new TextEditingController();
  TextEditingController _weightController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("BMI Calculatore"),
        centerTitle: true,
      ),
      body: new ListView(
        children: <Widget>[
          new Image.asset(
            'images/bmilogo.png',
            height: 133.0,
            width: 200.0,
          ),
          new Container(
            alignment: Alignment.center,
            child: new Column(
              children: <Widget>[
                new TextField(
                  controller: _ageController,
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                      hintText: 'Age', icon: new Icon(Icons.person_outline)),
                ),
                new TextField(
                  controller: _heightController,
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                      hintText: 'Height in feet',
                      icon: new Icon(Icons.view_headline)),
                ),
                new TextField(
                  controller: _weightController,
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                      hintText: 'Weight in lb',
                      icon: new Icon(Icons.line_weight)),
                ),
                new Padding(padding: EdgeInsets.all(3.0)),
                new RaisedButton(
                  child: new Text(
                    "Calculate",
                    style: new TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w800),
                  ),
                  color: Colors.pinkAccent,
                  onPressed: _onCalculateButtonClicked ,
                )
              ],
            ),
          ),
          new Container(
            alignment: Alignment.center,
            child: new Column(
              children: <Widget>[
                // result
                new Text(
                  "Your BMI: $_formattedBmi",
                  style: new TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 14.0,
                      color: Colors.blue),
                ),

                new Text(
                  "$_weightType",
                  style: new TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 14.0,
                      color: Colors.pinkAccent),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  double _bmi = 0;
  String _weightType;
  String _formattedBmi = "";
  void _onCalculateButtonClicked() {
    if (_ageController.text.isNotEmpty &&
        _heightController.text.isNotEmpty &&
        _weightController.text.isNotEmpty) {
      int age = int.parse(_ageController.text);
      double height = double.parse(_heightController.text);
      double weight = double.parse(_weightController.text);

      double inch = height * 12;
      setState(() {
        _bmi = weight / (inch * inch) * 703;
        _formattedBmi = _bmi.toStringAsFixed(1);

        if(_bmi < 18.5) {
          _weightType = "Underweight";
        } else if(_bmi >= 18.5 && _bmi < 25) {
          _weightType = "Great shape!";
        } else if(_bmi > 25.0 && _bmi < 30) {
          _weightType = "Overweight";
        } else {
          _weightType = "Obese";
        }
      });
    }
  }
}
