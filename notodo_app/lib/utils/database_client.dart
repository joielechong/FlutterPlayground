import 'dart:async';
import 'dart:io';

import 'package:notodo_app/model/nodo_item.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  final String ourTable = "nodoTbl";
  final String columnId = "id";
  final String columnItemName = "itemName";
  final String columnCreated = "dateCreated";

  Future<Database> get db async {
    if (_db == null) {
      _db = await initDb();
    }

    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "notodo.db");

    // Make sure the directory exists
    try {
      await Directory(databasesPath).create(recursive: true);
    } catch (_) {}

    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE $ourTable($columnId INTEGER PRIMARY KEY, $columnItemName TEXT, $columnCreated TEXT)");
  }

  Future<int> saveItem(NoDoItem item) async {
    var dbClient = await db;

    int res = await dbClient.insert(ourTable, item.toMap());
    return res;
  }

  Future<List> getItems() async {
    var dbClient = await db;
    String sql = "SELECT * FROM $ourTable ORDER BY $columnItemName ASC";
    var result = await dbClient.rawQuery(sql);

    return result.toList();
  }

  Future<int> getCount() async {
    var dbClient = await db;
    String sql = "SELECT COUNT(*) FROM $ourTable";
    return Sqflite.firstIntValue(await dbClient.rawQuery(sql));
  }

  Future<NoDoItem> getItem(int id) async {
    var dbClient = await db;
    String sql = "SELECT * FROM $ourTable WHERE $columnId = $id";
    var result = await dbClient.rawQuery(sql);
    if (result.length == 0) return null;
    return new NoDoItem.fromMap(result.first);
  }

  Future<int> deleteItem(int id) async {
    var dbClient = await db;

    return await dbClient
        .delete(ourTable, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> updateItem(NoDoItem item) async {
    var dbClient = await db;
    return await dbClient.update(ourTable,
    item.toMap(), where: "$columnId = ?", whereArgs: [item.id]);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
