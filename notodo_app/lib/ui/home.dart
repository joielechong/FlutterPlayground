import 'package:flutter/material.dart';

import 'NoTodoScreen.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("No TODO"),
        backgroundColor: Colors.black54,
      ),

      body: new NoToDoScreen(),
    );
  }
}
