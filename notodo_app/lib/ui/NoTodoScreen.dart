import 'package:flutter/material.dart';
import 'package:notodo_app/model/nodo_item.dart';
import 'package:notodo_app/utils/database_client.dart';
import 'package:notodo_app/utils/date_formatter.dart';

class NoToDoScreen extends StatefulWidget {
  @override
  _NoToDoScreenState createState() => _NoToDoScreenState();
}

class _NoToDoScreenState extends State<NoToDoScreen> {
  var _textEditingController = new TextEditingController();
  var db = new DatabaseHelper();

  final List<NoDoItem> _itemList = <NoDoItem>[];

  @override
  void initState() {
    super.initState();

    _readNoDoList();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.black87,
      body: new Column(
        children: <Widget>[
          new Flexible(
            child: new ListView.builder(
              itemBuilder: (_, int index) {
                return new Card(
                  color: Colors.white10,
                  child: new ListTile(
                    title: _itemList[index],
                    onLongPress: () => _updateItem(_itemList[index], index),
                    trailing: new Listener(
                      key: new Key(_itemList[index].itemName),
                      child: new Icon(
                        Icons.remove_circle,
                        color: Colors.redAccent,
                      ),
                      onPointerDown: (pointerEvent) =>
                          deleteNoDo(_itemList[index].id, index),
                    ),
                  ),
                );
              },
              padding: new EdgeInsets.all(8.0),
              reverse: false,
              itemCount: _itemList.length,
            ),
          ),
          new Divider(
            height: 1.0,
          )
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        tooltip: "Add Item",
        onPressed: _showFormDialog,
        child: new ListTile(
          title: new Icon(Icons.add),
        ),
      ),
    );
  }

  void _showFormDialog() {
    var dialog = new AlertDialog(
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                controller: _textEditingController,
                autofocus: true,
                decoration: new InputDecoration(
                    labelText: "Item",
                    hintText: "eg. Don't buy stuff",
                    icon: new Icon(Icons.note_add)),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              onPressed: () {
                _handleSubmit(_textEditingController.text);
              },
              child: Text("Save")),
          new FlatButton(
              onPressed: () => Navigator.pop(context), child: Text("Cancel"))
        ]);

    showDialog(
        context: context,
        builder: (_) {
          return dialog;
        });
  }

  _handleSubmit(String text) async {
    _textEditingController.clear();
    NoDoItem item = new NoDoItem(text, dateFormatted());
    int savedItemId = await db.saveItem(item);

    NoDoItem addedItem = await db.getItem(savedItemId);

    setState(() {
      _itemList.insert(0, addedItem);
    });

    Navigator.pop(context);
  }

  _readNoDoList() async {
    List items = await db.getItems();
    items.forEach((item) {
      NoDoItem noDoItem = NoDoItem.fromMap(item);
      print("DB items: ${noDoItem.itemName}");
      _itemList.add(noDoItem);
    });

    setState(() {
      _itemList.length;
    });
  }

  deleteNoDo(int itemId, int index) async {
    debugPrint("Deleted item!");

    await db.deleteItem(itemId);
    setState(() {
      _itemList.removeAt(index);
    });
  }

  _updateItem(NoDoItem item, int index) {
    var dialog = new AlertDialog(
      title: Text('Update Item'),
      content: new Row(
        children: <Widget>[
          new Expanded(
            child: new TextField(
              controller: _textEditingController,
              autofocus: true,
              decoration: new InputDecoration(
                  labelText: "Item",
                  hintText: "eg. Don't buy stuff",
                  icon: Icon(Icons.update)),
            ),
          )
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Update'),
          onPressed: () async {
            NoDoItem itemUpdated = NoDoItem.fromMap({
              "itemName": _textEditingController.text,
              "id": item.id,
              "dateCreated": dateFormatted()
            });
            
            _handleSubmittedUpdate(index, item);
            await db.updateItem(itemUpdated);

            setState(() {
              _readNoDoList();
            });

            Navigator.of(context).pop();

          },
        ),
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("Cancel"),
        )
      ],
    );

    showDialog(
        context: context,
        builder: (_) {
          return dialog;
        });
  }

  void _handleSubmittedUpdate(int index, NoDoItem item) {
    setState(() {
      _itemList.removeWhere((element) {
        _itemList[index].itemName == item.itemName;
      });
    });
  }
}
