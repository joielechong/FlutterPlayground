import 'package:flutter/material.dart';

import '../home.dart';

void main() {
  String title = "Gesture";
  runApp(
    new MaterialApp(
      title: title,
        home: new Home(title: title,),
    )
  );
}