import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class QuakeWidget extends StatefulWidget {
  List _data;

  QuakeWidget(this._data);

  @override
  State<StatefulWidget> createState() {
    return new QuakeWidgetState(_data);
  }
}

class QuakeWidgetState extends State<QuakeWidget> {
  List _data;

  QuakeWidgetState(this._data);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Quake App"),
        backgroundColor: Colors.redAccent,
      ),
      body: new Center(
        child: new ListView.builder(
            itemBuilder: (BuildContext context, int position) {
          return new ListTile(
            title: new Text(
              _getFormattedDate(
                  int.parse("${_data[position]['properties']['time']}")),
              style: new TextStyle(fontSize: 18.0, color: Colors.orangeAccent),
            ),
            subtitle: new Text(
              "${_data[position]['properties']['place']}",
              style: new TextStyle(fontSize: 13.4, color: Colors.grey),
            ),
            leading: new CircleAvatar(
              backgroundColor: Colors.green,
              child: new Text(
                "${_data[position]['properties']['mag']}",
                style: new TextStyle(fontSize: 13.4, color: Colors.white),
              ),
            ),
            onTap: () {
              _showDialogForDetails("${_data[position]['properties']['mag']}" + "-"
                  + "${_data[position]['properties']['place']}" );
            },
          );
        }),
      ),
    );
  }

  String _getFormattedDate(int time) {
    time = time * 1000;
    return new DateFormat()
        .format(new DateTime.fromMicrosecondsSinceEpoch(time));
  }

  void _showDialogForDetails(String message) {
    var alert = new AlertDialog(
      title: new Text("Quake"),
      content: new Text(message),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: new Text('OK'),
        )
      ],
    );

    showDialog(context: context, builder: (_)=> alert);
  }
}
