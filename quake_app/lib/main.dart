import 'package:flutter/material.dart';
import 'package:quake_app/ui/QuakeWidget.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() async {
  Map _data = await getQuake();
  print(_data['features']);

  runApp(new MaterialApp(
    title: "Quake App",
    home: new QuakeWidget(_data['features']),
  ));
}


Future<Map> getQuake() async {
  String apiUrl = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson';
  http.Response response = await http.get(apiUrl);

  return json.decode(response.body);
}