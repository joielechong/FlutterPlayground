import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stateless_widget/ui/create_profile.dart';

void main() {
  runApp(new MaterialApp(
    home: HomeScreen(),
    debugShowCheckedModeBanner: false,
  ));
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarIconBrightness: Brightness.light));

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'images/background.jpg',
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
          ),
          Container(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(15),
                ),
                Text(
                  "Teacher's\n Corner",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Roboto',
                      color: Colors.green),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                ),
                Text(
                  "An app to meet other teachers and share resources",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Roboto',
                      color: Colors.white),
                ),
                Padding(
                  padding: EdgeInsets.all(60),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    OutlineButton(
                        child: Text(
                          "Sign up",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        padding: const EdgeInsets.all(5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    CreateProfile()))), // OutlineButton

                    OutlineButton(
                        child: Text(
                          "Sign ins",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        padding: const EdgeInsets.all(5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () =>
                            debugPrint("Sign in pressed")), // OutlineButton
                  ],
                ),
                Padding(padding: EdgeInsets.all(60)),
                InkWell(
                  onTap: () => debugPrint("Skip Button Pressed"),
                  child: Text(
                    "Skip this for now!>>>",
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: 'Roboto',
                        color: Colors.white),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
