import 'package:flutter/material.dart';

class CreateProfile extends StatefulWidget {
  @override
  _CreateProfileState createState() => _CreateProfileState();
}

class _CreateProfileState extends State<CreateProfile> {
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Create a Profile'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Stack(
        children: <Widget>[
          Form(
            child: ListView(
              children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.text,
                  controller: _userController,
                  decoration: InputDecoration(
                      hintText: 'Enter a User Name',
                      contentPadding: const EdgeInsets.all(20),
                      labelText: 'User Name'),
                ),
                TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(20),
                      hintText: 'you@example.com',
                      labelText: 'Email address'),
                ),
              ],
            ),
          ), // Form

          Positioned(
            left: 5.0,
            right: 5.0,
            bottom: 5.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  // TextStyle, Text
                  padding: EdgeInsets.all(5.0),
                  color: Colors.deepPurple,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  onPressed: () => debugPrint('Submit button is pressed'),
                ),
                RaisedButton(
                    child: Text(
                      'Reset',
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                    // TextStyle, Text
                    padding: EdgeInsets.all(5.0),
                    color: Colors.deepPurple,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    onPressed: () {
                      setState(() {
                        _userController.clear();
                        _emailController.clear();
                      });
                    })
              ],
            ),
          )
        ],
      ),
    );
  }
}
