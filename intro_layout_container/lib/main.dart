
import 'package:flutter/material.dart';
import 'package:intro_layout_container/ui/home.dart';

void main() {
  runApp(new MaterialApp(
    title: "Layouts",
    home: new Home(),
  ));
}