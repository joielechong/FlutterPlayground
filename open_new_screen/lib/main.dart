import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(title: "Screens", home: new Home()));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _nameFieldController = new TextEditingController();

  _goToNextScreen(BuildContext context) async {
    Map result = await Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return NextScreen(name: _nameFieldController.text);
    }));

    if (result.containsKey("info")) {
      _nameFieldController.text = result["info"];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("First screen"),
        centerTitle: true,
        backgroundColor: Colors.greenAccent,
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: TextField(
              controller: _nameFieldController,
              decoration: InputDecoration(labelText: 'Enter your name'),
            ),
          ),
          ListTile(
            title: RaisedButton(
                child: Text("Send to next screen"),
                onPressed: () {
//                var router = MaterialPageRoute(
//                  builder: (BuildContext context) {
//                    return NextScreen(name: _nameFieldController.text);
//                  }
//                );
//
//                Navigator.of(context).push(router);
                  _goToNextScreen(context);
                }),
          )
        ],
      ),
    );
  }
}

class NextScreen extends StatefulWidget {
  final String name;

  NextScreen({Key key, this.name}) : super(key: key);

  @override
  _NextScreenState createState() => _NextScreenState();
}

class _NextScreenState extends State<NextScreen> {
  var _backTextFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text("Second screen"),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(widget.name),
            ),
            ListTile(
              title: TextField(
                controller: _backTextFieldController,
              ),
            ),
            ListTile(
              title: FlatButton(
                onPressed: () {
                  Navigator.pop(
                      context, {'info': _backTextFieldController.text});
                },
                child: Text("Send data back"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
