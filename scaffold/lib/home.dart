import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.greenAccent.shade700,
        title: new Text("Great Day"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.send), onPressed: () => debugPrint("")),
          new IconButton(
            icon: new Icon(Icons.search),
            onPressed: _onPress,
          )
        ],
      ),

      // other properties
      backgroundColor: Colors.grey.shade100,
      body: new Container(
        alignment: Alignment.center,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              "Text One",
              style: new TextStyle(
                  fontSize: 14.5,
                  fontWeight: FontWeight.w400,
                  color: Colors.deepPurple),
            ),

            new InkWell(
              child: new Text("Button!"),
              onTap: () => debugPrint("Button Tapped!"),
            )
          ],
        ),
      ),

      bottomNavigationBar:new BottomNavigationBar(items:[
        new BottomNavigationBarItem(icon: new Icon(Icons.add), title: new Text("Hey")),
        new BottomNavigationBarItem(icon: new Icon(Icons.print), title: new Text("Hoi")),
        new BottomNavigationBarItem(icon: new Icon(Icons.call_missed), title: new Text("Oui")),
      ], onTap: (int i) => debugPrint("Hey Touched $i"),),

      floatingActionButton: new FloatingActionButton(
        onPressed: () => debugPrint("Pressed!"),
          backgroundColor: Colors.lightGreen,
          tooltip: "Going up!",
          child: new Icon(Icons.call_missed),
      ),
    );
  }

  void _onPress() {
    print("Search tapped");
  }
}
